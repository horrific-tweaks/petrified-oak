This datapack + resource pack adds textures and functionality for the unobtainable [Petrified Oak Slab](https://minecraft.fandom.com/wiki/Slab#:~:text=Petrified%20oak%20slabs%20are) that exists in the game. The texture appears as a desaturated glossy purple variant of the wooden planks texture, with a darker deepslate-like top surface similar to the deepslate blocks.

![Petrified oak slabs placed alongside a variety of other textures](./.images/textures.png)

## Obtaining

With the datapack installed, petrified oak can be obtained by placing any nether planks (crimson or warped) next to water, on top of a stone or dirt block. The planks will gradually age into petrified planks over time (averaging 2-3 minutes per block).

![An example of how to generate petrified planks in the correct conditions](./.images/generation.png)

Mining these blocks with silk touch will yield a `Petrified Wood Planks` item; without silk touch, it will yield two `Petrified Wood Slab` items.
