<% for block, blockinfo in aging.blocks %>
execute as @a if score @s << blockinfo.scoreboard >> matches 1.. at @s run function fennifith:petrified_oak/raycast/start
scoreboard players set @a << blockinfo.scoreboard >> 0
<% endfor %>

execute as @a if score @s ptr.water matches 1.. at @s run function fennifith:petrified_oak/raycast/start
scoreboard players set @a ptr.water 0
