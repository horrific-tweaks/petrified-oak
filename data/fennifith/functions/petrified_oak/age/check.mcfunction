scoreboard players set #valid ptr.cast 0

# check that current block type can age
<% for block, blockinfo in aging.blocks %>
execute if block ~ ~ ~ << block >> run scoreboard players set #valid ptr.cast 1
<% endfor %>

# check surrounding/adjacent block conditions
scoreboard players set #valid_adj ptr.cast 0
execute if block ~1 ~ ~ #fennifith:petrified_oak/root_adj run scoreboard players set #valid_adj ptr.cast 1
execute if block ~-1 ~ ~ #fennifith:petrified_oak/root_adj run scoreboard players set #valid_adj ptr.cast 1
execute if block ~ ~ ~1 #fennifith:petrified_oak/root_adj run scoreboard players set #valid_adj ptr.cast 1
execute if block ~ ~ ~-1 #fennifith:petrified_oak/root_adj run scoreboard players set #valid_adj ptr.cast 1
execute unless block ~ ~-1 ~ #fennifith:petrified_oak/root run scoreboard players set #valid_adj ptr.cast 0
# merge with #valid condition
execute if score #valid_adj ptr.cast matches 0 run scoreboard players set #valid ptr.cast 0

# if block is valid, summon new marker entity
execute if score #valid ptr.cast matches 1 unless entity @e[type=area_effect_cloud,tag=ptr.marker,distance=..0.5] run summon area_effect_cloud ~ ~ ~ {Tags:["ptr.marker"],Invisible:1b,Invulnerable:1b,PersistenceRequired:1b,NoGravity:1b,Marker:1b,Duration:2147483637}
# if block is not valid, remove marker entity
execute unless score #valid ptr.cast matches 1 run kill @e[type=area_effect_cloud,tag=ptr.marker,distance=..0.5]

# if block is water, check surrounding blocks for valid locations
execute if score #chk_water ptr.cast matches 1 if block ~ ~ ~ water run function fennifith:petrified_oak/age/check_water

