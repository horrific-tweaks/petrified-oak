<% for block, blockinfo in aging.blocks %>
<% if blockinfo.tags %>
<% for tags in blockinfo.tags %>
execute if block ~ ~ ~ << block >>[<< tags >>] run setblock ~ ~ ~ minecraft:petrified_oak_slab[<< tags >>]
<% endfor %>
<% else %>
execute if block ~ ~ ~ << block >> run setblock ~ ~ ~ minecraft:petrified_oak_slab[type=double]
<% endif %>
<% endfor %>
