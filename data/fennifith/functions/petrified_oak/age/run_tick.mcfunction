scoreboard players set #chk_water ptr.cast 0
execute as @e[type=area_effect_cloud,tag=ptr.marker] at @s run function fennifith:petrified_oak/age/check

execute as @e[type=area_effect_cloud,tag=ptr.marker,tag=ptr.aged1,sort=random,limit=1] at @s run function fennifith:petrified_oak/age/replace
execute as @e[type=area_effect_cloud,tag=ptr.marker,tag=ptr.aged0] run tag @s add ptr.aged1
execute as @e[type=area_effect_cloud,tag=ptr.marker] run tag @s add ptr.aged0

schedule function fennifith:petrified_oak/age/run_tick << aging.tick >> replace
