tag @s add ptr.raycasting

scoreboard players set #hit ptr.cast 0
scoreboard players set #distance ptr.cast 0
execute as @s at @s anchored eyes positioned ^ ^ ^ anchored feet run function fennifith:petrified_oak/raycast/ray

tag @s remove ptr.raycasting
execute if score #hit ptr.cast matches 1 run schedule function fennifith:petrified_oak/age/run_tick << aging.tick >> replace
