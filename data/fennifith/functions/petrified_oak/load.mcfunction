<% for block, blockinfo in aging.blocks %>
scoreboard objectives add << blockinfo.scoreboard >> minecraft.used:minecraft.<< block >>
scoreboard objectives add << blockinfo.scoreboard >> minecraft.used:minecraft.<< block >>
<% endfor %>

scoreboard objectives add ptr.water minecraft.used:minecraft.water_bucket
scoreboard objectives add ptr.cast dummy

schedule function fennifith:petrified_oak/age/run_tick << aging.tick >> replace
